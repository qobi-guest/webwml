#use wml::debian::translation-check translation="1cd443bbff5c5f8e18b9961d25d41c997d4a4103" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Heimdal, une
implémentation de Kerberos 5 qui vise à être compatible avec la version
Kerberos du MIT.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16860">CVE-2018-16860</a>

<p>Isaac Boukris et Andrew Bartlett ont découvert que Heimdal était
vulnérable à des attaques de type « homme du milieu » provoquées par une
validation incomplète des sommes de contrôle. Vous trouverez des détails
sur ce problème dans l'annonce de Samba à l'adresse
<a href="https://www.samba.org/samba/security/CVE-2018-16860.html">\
https://www.samba.org/samba/security/CVE-2018-16860.html</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12098">CVE-2019-12098</a>

<p>Un échec de vérification de l'échange de clés PA-PKINIT-KX côté client
pourrait permettre de réaliser une attaque de type « homme du milieu ».</p></li>

</ul>

<p>Pour la distribution stable (Stretch), ces problèmes ont été corrigés
dans la version 7.1.0+dfsg-13+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets heimdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de heimdal, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4455.data"
# $Id: $
