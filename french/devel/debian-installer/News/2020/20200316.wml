#use wml::debian::translation-check translation="72b16d926fb3d9d347786aeacecb99bf094edb14" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de l'installateur Debian Bullseye Alpha 2</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
L'<a href="https://wiki.debian.org/DebianInstaller/Team">équipe</a> du
programme d'installation de Debian a le plaisir d'annoncer la parution
de la deuxième version candidate pour Debian 11 <q>Bullseye</q>.
</p>


<h2>Améliorations dans cette version de l'installateur</h2>

<ul>
  <li>anna :
    <ul>
      <li>transformation de l'avertissement concernant l'absence de modules du
        noyau en une erreur plus descriptive (<a href="https://bugs.debian.org/749991">nº 749991</a>,
        <a href="https://bugs.debian.org/367515">nº 367515</a>).</li>
    </ul>
  </li>
  <li>clock-setup :
    <ul>
      <li>réécriture des messages sur les avantages d'une configuration
        correcte de l'heure.</li>
    </ul>
  </li>
  <li>espeakup :
    <ul>
      <li>pas d'utilisation de pause pour des durées inférieures à la seconde ;</li>
      <li>affichage du nombre de cartes détectées.</li>
    </ul>
  </li>
  <li>debian-installer :
    <ul>
      <li>write-built-using rendu plus robuste ;</li>
      <li>passage de l'ABI du noyau Linux à la version 5.4.0-4.</li>
    </ul>
  </li>
  <li>glibc :
    <ul>
      <li>dépendance de libc-udeb à libcrypt1-udeb (<a href="https://bugs.debian.org/941853">nº 941853</a>).</li>
    </ul>
  </li>
  <li>grub-installer :
    <ul>
      <li>modification des messages pour clarifier la définition du système
        Debian qui vient d'être installé.</li>
    </ul>
  </li>
  <li>netcfg :
    <ul>
      <li>amélioration des messages (en particulier en ce qui concerne des
        adresses IP mal formées).</li>
    </ul>
  </li>
  <li>pkgsel :
    <ul>
      <li>installation de tasksel assurée, quelle que soit sa priorité ;</li>
      <li>ajout de messages de debconf de <code>pkgsel/run_tasksel</code>
        préconfigurable, permettant (avec le réglage à <code>false</code>)
        d'ignorer complètement tasksel (installation et invite), tout en
        bénéficiant des autres fonctions de pkgsel.</li>
    </ul>
  </li>
  <li>preseed :
    <ul>
      <li>mise à jour de <code>auto-install/defaultroot</code>, remplaçant
        Buster par Bullseye.</li>
    </ul>
  </li>
  <li>rootskel :
    <ul>
      <li>réorganisation du script pour assurer que les utilisateurs soient
        configurés ;</li>
      <li>activation du thème à contraste élevé quand l'installation a été
        réalisée avec le thème sombre ;</li>
      <li>activation des fonctions ezoom de compiz pour l'accessibilité ;</li>
      <li>indication du champ fs_spec dans les points de montage de
        fstab-linux. Certains outils analysent /proc/mounts et essayent de le
        mettre en correspondance avec le champ spec pour trouver où sysfs est
        monté (c'est-à-dire dasdfmt sur s390x) ; cela fait que l'environnement
        de l'installateur ressemble plus aux systèmes installés ;</li>
      <li>ajustement de l'utilisation des consoles multiples. En cas de
        détection de préconfiguration, pas d'exécution sur plusieurs consoles
        en parallèle parce que cela provoque des situations de compétition et
        des comportements bizarres. Exécution seulement sur la console
        « préférée » (<a href="https://bugs.debian.org/940028">nº 940028</a>, <a href="https://bugs.debian.org/932416">nº 932416</a>).</li>
    </ul>
  </li>
  <li>systemd :
    <ul>
      <li>utilisation de 73-usb-net-by-mac.link dans udev-udeb (voir aussi :
        <a href="https://bugs.debian.org/946196">nº 946196</a>).</li>
    </ul>
  </li>
  <li>user-setup :
    <ul>
      <li>ajout de « input », « kvm » et « render » à la liste des noms
        réservés ; udev.postinst les ajoute comme groupes système.</li>
    </ul>
  </li>
</ul>


<h2>Modifications de la prise en charge matérielle</h2>

<ul>
  <li>debian-installer :
    <ul>
      <li>passage de vmlinux à vmlinuz pour mips* sauf sur Octeon ;</li>
      <li>mise à jour de l'image Firefly-RK3288 pour la nouvelle version
        d'u-boot.</li>
    </ul>
  </li>
  <li>flash-kernel :
    <ul>
      <li>ajout de la prise en charge des entrées bootspec, amorçable par
        Barebox (<a href="https://bugs.debian.org/931953">nº 931953</a>).</li>
      <li>correction d'un cas exceptionnel qui provoque l'échec silencieux de
        flash-kernel
        (<a href="https://bugs.debian.org/932231">nº 932231</a>) ;</li>
      <li>initrd rendu optionnel pour les machines avec Boot-Multi-Path
        (<a href="https://bugs.debian.org/869073">nº 869073</a>) ;</li>
      <li>ajout de la prise en charge du kit de développement de Librem 5
        (<a href="https://bugs.debian.org/927700">nº 927700</a>) ;</li>
      <li>ajout de la prise en charge des ordinateurs portables OLPC XO-1.75.</li>
    </ul>
  </li>
</ul>


<h2>État de la localisation</h2>

<ul>
  <li>76 langues sont prises en charge dans cette version.</li>
  <li>La traduction est complète pour douze de ces langues.</li>
</ul>


<h2>Problèmes connus dans cette version</h2>

<p>
Veuillez consulter les <a href="$(DEVEL)/debian-installer/errata">errata</a>
pour plus de détails et une liste complète des problèmes connus.
</p>


<h2>Retours d'expérience pour cette version</h2>

<p>
Nous avons besoin de votre aide pour trouver des bogues et améliorer encore
l'installateur, merci de l'essayer. Les CD, les autres supports d'installation,
et tout ce dont vous pouvez avoir besoin sont disponibles sur notre
<a href="$(DEVEL)/debian-installer">site web</a>.
</p>


<h2>Remerciements</h2>

<p>
L'équipe du programme d'installation Debian remercie toutes les personnes ayant
pris part à cette publication.
</p>
