#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Poppler, une bibliothèque de rendu de PDF, était affectée par plusieurs bogues
de déni de service (plantage d'application), de déréférencement de pointeur NULL
et de corruption de mémoire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14517">CVE-2017-14517</a>

<p>Déréférencement de pointeur NULL dans la fonction XRef::parseEntry() dans
XRef.cc</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14519">CVE-2017-14519</a>

<p>Corruption de mémoire dans l’appel à Object::streamGetChar pouvant conduire à
un déni de service ou un autre impact non précisé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14617">CVE-2017-14617</a>

<p>Dépassement potentiel de tampon dans la classe ImageStream dans Stream.cc
pouvant conduire à un déni de service ou à un autre impact non précisé.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 0.18.4-6+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1116.data"
# $Id: $
