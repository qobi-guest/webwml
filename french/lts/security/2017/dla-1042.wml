#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9122">CVE-2017-9122</a>

<p>La fonction quicktime_read_moov dans moov.c dans libquicktime 1.2.4 permet à
des attaquants distants de provoquer un déni de service (boucle infinie et
consommation de CPU) à l'aide d'un fichier mp4 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9123">CVE-2017-9123</a>

<p>La fonction lqt_frame_duration dans lqt_quicktime.c dans libquicktime 1.2.4
permet à des attaquants distants de provoquer un déni de service (lecture de
mémoire non valable et plantage d'application) à l'aide d'un fichier mp4
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9124">CVE-2017-9124</a>

<p>La fonction quicktime_match_32 dans util.c dans libquicktime 1.2.4 permet à
des attaquants distants de provoquer un déni de service (déréférencement de
pointeur NULL et plantage d'application) à l'aide d'un fichier mp4 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9125">CVE-2017-9125</a>

<p>La fonction lqt_frame_duration dans lqt_quicktime.c dans libquicktime 1.2.4
permet à des attaquants distants de provoquer un déni de service (lecture hors
limites de tampon basé sur le tas) à l'aide d'un fichier mp4 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9126">CVE-2017-9126</a>

<p>La fonction quicktime_read_dref_table dans dref.c dans libquicktime 1.2.4
permet à des attaquants distants de provoquer un déni de service (dépassement de
tampon basé sur le tas et plantage d'application) à l'aide d'un fichier mp4
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9127">CVE-2017-9127</a>

<p>La fonction quicktime_user_atoms_read_atom dans useratoms.c dans
libquicktime 1.2.4 permet à des attaquants distants de provoquer un déni de
service (dépassement de tampon basé sur le tas et plantage d'application) à
l'aide d'un fichier mp4 contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9128">CVE-2017-9128</a>

<p>La fonction quicktime_video_width dans lqt_quicktime.c dans
libquicktime 1.2.4 permet à des attaquants distants de provoquer un déni de
service (lecture hors limites de tampon basé sur le tas et plantage
d'application) à l'aide d'un fichier mp4 contrefait.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2:1.2.4-3+deb7u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libquicktime.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1042.data"
# $Id: $
