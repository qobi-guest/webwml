#use wml::debian::translation-check translation="55d1ac616e9ec6fe42ad1680e45c2ce133b85547" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans l'hyperviseur Xen. Le
projet « Common Vulnerabilities and Exposures » (CVE) identifie les
problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9379">CVE-2016-9379</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9380">CVE-2016-9380</a> (XSA-198)

<p>pygrub, l'émulateur de chargeur de démarrage, échouait à évaluer ou
vérifier la sécurité de ses résultats lors de leur rapport à l'appelant. Un
administrateur client malveillant peut obtenir le contenu de fichiers
sensibles de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9381">CVE-2016-9381</a> (XSA-197)

<p>Le compilateur peut émettre des optimisations dans qemu qui peuvent
conduire à des vulnérabilités de type <q>double fetch</q>. Des
administrateurs malveillants peuvent exploiter cette vulnérabilité pour
prendre le contrôle du processus de qemu, en élevant ses privilèges à celui
du processus de qemu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9382">CVE-2016-9382</a> (XSA-192)

<p>Le LDTR, exactement comme le TR, est strictement une fonctionnalité en
mode protégé. Par conséquent, lors d'un passage vers le mode VM86, le
chargement du LDTR doit suivre la sémantique du mode protégé. Un processus
client non privilégié malveillant peut planter ou élever ses privilèges à
ceux du système d'exploitation client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9383">CVE-2016-9383</a> (XSA-195)

<p>Quand Xen a besoin d'émuler certaines instructions, pour gérer de façon
efficace l'émulation, l'adresse mémoire et l'opérande du registre sont
recalculés en interne par Xen. Durant ce processus, les bits d'ordre
supérieur d'une expression intermédiaire sont éliminés, aboutissant à ce
qu'à la fois l'emplacement mémoire et l'opérande du registre soient faux.
Un client malveillant peut modifier un emplacement mémoire arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9386">CVE-2016-9386</a> (XSA-191)

<p>L'émulateur Xen x86 échouait de façon erronée à considérer comme
inutilisables des segments lors des accès mémoire. Un programme utilisateur
client non privilégié peut être capable d'élever ses droits à ceux du
système d'exploitation hôte.</p>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4.1.6.lts1-4. Pour Debian 8 <q>Jessie</q>, ces problèmes seront
corrigés bientôt.</p>

<p>Nous vous recommandons de mettre à jour vos paquets xen.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-720.data"
# $Id: $
