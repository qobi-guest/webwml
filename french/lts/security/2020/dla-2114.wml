#use wml::debian::translation-check translation="066e3041caa32e3fccf234cb4344d837a12ca5c2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou
une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13093">CVE-2018-13093</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-13094">CVE-2018-13094</a>

<p>Wen Xu du SSLab de Gatech a signalé plusieurs défauts potentiels de
déréférencement de pointeur NULL pouvant être déclenchés lors du montage et de
l’utilisation d’un volume XFS. Un attaquant capable de monter des volumes XFS
contrefaits pourrait utiliser cela pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20976">CVE-2018-20976</a>

<p>L’implémentation du système de fichiers XFS ne gérait pas correctement
certaines conditions d’échec de montage. Cela pourrait conduire à une
utilisation de mémoire après libération. L’impact de sécurité n’est pas évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-21008">CVE-2018-21008</a>

<p>Le pilote wifi rsi ne gérait pas correctement certaines conditions d’échec.
Cela pourrait conduire à utilisation de mémoire après libération. L’impact de
sécurité n’est pas évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-0136">CVE-2019-0136</a>

<p>La mise en œuvre de soft-MAC (mac80211) wifi ne certifiait pas correctement
les messages TDLS (Tunneled Direct Link Setup). Un attaquant proche pourrait
utiliser cela pour provoquer un déni de service (perte de la connectivité wifi).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-2215">CVE-2019-2215</a>

<p>L’outil syzkaller a découvert une vulnérabilité d’utilisation de
mémoire après libération dans le pilote binder d’Android. Un utilisateur local
sur un système avec ce pilote activé pourrait utiliser cela pour provoquer
un déni de service (corruption de mémoire ou plantage) ou éventuellement pour
une élévation des privilèges. Cependant, ce pilote n’est pas activé dans les
paquets de noyaux de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10220">CVE-2019-10220</a>

<p>Divers développeurs et chercheurs ont trouvé que si un système de fichiers
contrefait ou un serveur de fichiers malveillant fournissait un répertoire avec
des noms de fichier incluant des caractères « / », cela pourrait dérouter et
éventuellement contrecarrer les vérifications de sécurité dans des applications
lisant le répertoire.</p>

<p>Le noyau renverra désormais une erreur lors de la lecture d’un tel répertoire,
plutôt que de passer des noms de fichier non valables dans l’espace utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14615">CVE-2019-14615</a>

<p>Il a été découvert que les GPU d’Intel de neuvième et dixième génération ne
nettoyait pas l’état visible par l’utilisateur lors du changement de contexte. Cela
aboutissait à une fuite d'informations entre des tâches de GPU. Cela a été
atténué dans le pilote i915.</p>

<p>Les puces touchées (gén. 9 et gén. 10) sont listées sur
<url "https://en.wikipedia.org/wiki/List_of_Intel_graphics_processing_units#Gen9">.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14814">CVE-2019-14814</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14815">CVE-2019-14815</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14816">CVE-2019-14816</a>

<p>Plusieurs bogues ont été découverts dans le pilote wifi mwifiex qui pourraient
conduire à des dépassements de tampon basé sur le tas. Un utilisateur local,
autorisé à configurer un périphérique géré par ce pilote, pourrait probablement
utiliser cela pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14895">CVE-2019-14895</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14901">CVE-2019-14901</a>

<p>ADLab de Venustech a découvert des dépassements potentiels de tampon de tas
dans le pilote wifi mwifiex. Sur des systèmes utilisant ce pilote, un point
d’accès sans fil malveillant ou un pair adhoc/P2P pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14896">CVE-2019-14896</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-14897">CVE-2019-14897</a>

<p>ADLab de Venustech a découvert des dépassements potentiels de tampons de tas et
de pile dans le pilote wifi libertas. Sur des systèmes utilisant ce pilote, un point
d’accès sans fil malveillant ou un pair adhoc/P2P pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15098">CVE-2019-15098</a>

<p>Hui Peng et Mathias Payer ont signalé que le pilote wifi ath6kl ne validait
pas correctement les descripteurs USB, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15217">CVE-2019-15217</a>

<p>L’outil syzkaller a découvert que le pilote de média zr364xx ne gérait
pas correctement les périphériques sans chaîne de nom de fabricant, ce qui
pourrait conduire à un déréférencement de pointeur NULL. Un attaquant capable
d’ajouter des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15291">CVE-2019-15291</a>

<p>L’outil syzkaller a découvert que le pilote de média b2c2-flexcop-usb ne
validait pas correctement les descripteurs USB, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15505">CVE-2019-15505</a>

<p>L’outil syzkaller a découvert que le pilote de média technisat-usb2 ne
validait pas correctement les paquets IR entrants, ce qui pourrait conduire à
une lecture excessive de tampon de tas. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(bogue/oops) ou pour lire des informations sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15917">CVE-2019-15917</a>

<p>L’outil syzkaller a trouvé une situation de compétition dans le code de prise
en charge des adaptateurs Bluetooth liés UART. Cela pourrait conduire à une
utilisation après libération. Un utilisateur local, avec accès à un périphérique
pty ou un autre périphérique tty approprié, pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16746">CVE-2019-16746</a>

<p>Il a été découvert que la pile wifi ne validait pas le contenu des en-têtes
de balise de proximité fournies par l’espace utilisateur pour être utilisées sur
une interface wifi dans le mode « point d'accès », ce qui pourrait conduire à un
dépassement de tampon de tas. Un utilisateur local ayant le droit de configurer
une interface wifi pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17052">CVE-2019-17052</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17053">CVE-2019-17053</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17054">CVE-2019-17054</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17055">CVE-2019-17055</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-17056">CVE-2019-17056</a>

<p>Ori Nimron a signalé que diverses implémentations de protocole réseau — AX.25,
IEEE 802.15.4, Appletalk, ISDN et NFC — permettaient à tous les utilisateurs de
créer des sockets bruts. Un utilisateur local pourrait utiliser cela pour
envoyer des paquets arbitraires sur des réseaux utilisant ces protocoles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17075">CVE-2019-17075</a>

<p>Il a été découvert que le pilote cxgb4 Infiniband nécessitait DMA (Direct
Memory Access) à un tampon alloué de pile, ce qui n’est pas pris en charge et
dans quelques systèmes pouvait aboutir à une corruption de mémoire de la pile.
Un utilisateur local pouvait utiliser cela pour un déni de service (corruption
de mémoire ou plantage) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17133">CVE-2019-17133</a>

<p>Nicholas Waisman a signalé que la pile wifi ne validait pas les informations
SSID reçues avant de les copier, ce qui pourrait conduire à un dépassement de
tampon si cela n’était pas validé par le pilote ou le micrologiciel. Un point
d’accès sans fil malveillant pourrait être capable d’utiliser cela afin de provoquer
un déni de service (corruption de mémoire ou plantage) ou pour une exécution de
code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17666">CVE-2019-17666</a>

<p>Nicholas Waisman a signalé que le pilote wifi rtlwifi ne validait pas
correctement les informations P2P reçues, conduisant à un dépassement de tampon.
Un pair P2P malveillant pourrait utiliser cela afin de provoquer un déni de
service (corruption de mémoire ou plantage) ou pour une exécution de code à
distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18282">CVE-2019-18282</a>

<p>Jonathan Berger, Amit Klein et Benny Pinkas ont découvert que la génération
d’étiquettes de flux UDP/IPv6 utilisait une faible fonction de hachage,
<q>jhash</q>. Cela pourrait activer le suivi d’ordinateurs individuels dans
leurs communications avec différents serveurs distants et à partir de différents
réseaux. La fonction <q>siphash</q> est désormais utilisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18683">CVE-2019-18683</a>

<p>Plusieurs situations de compétition ont été découvertes dans le pilote de
média vivid, utilisé pour tester les applications Video4Linux2 (V4L2). Cela
pourrait aboutir à une utilisation de mémoire après libération. Dans un système
où ce pilote est chargé, un utilisateur avec permission d’accès aux périphériques
de média pourrait utiliser cela pour provoquer un déni de service (corruption de
mémoire ou plantage) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18809">CVE-2019-18809</a>

<p>Navid Emamdoost a découvert une fuite de mémoire potentielle dans le pilote
de média af9005 si le périphérique échoue à répondre à une commande. L’impact de
sécurité est peu évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19037">CVE-2019-19037</a>

<p>Il a été découvert que le pilote de système de fichiers ext4 ne gérait pas
les répertoires avec trous correctement (régions non allouées). Un attaquant
capable de monter des volumes ext4 arbitraires pourrait utiliser cela pour
provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19051">CVE-2019-19051</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
wimax i2400m si l’opération logicielle rfkill échoue. L’impact de sécurité n’est
pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19052">CVE-2019-19052</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
CAN  gs_usb si l’opération open (interface-up) échoue. L’impact de sécurité
n’est pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19056">CVE-2019-19056</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-19057">CVE-2019-19057</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
wifi mwifiex si l’opération probe échoue. L’impact de sécurité n’est pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19062">CVE-2019-19062</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le
sous-système AF_ALG si l’opération CRYPTO_MSG_GETALG échoue. Un utilisateur
local pourrait éventuellement utiliser cela afin de provoquer un déni de service
(épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19066">CVE-2019-19066</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
SCSI bfa si l’opération get_fc_host_stats échoue. L’impact de sécurité n’est pas
défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19068">CVE-2019-19068</a>

<p>Navid Emamdoost a découvert une fuite potentielle de mémoire dans le pilote
wifi rtl8xxxu, dans le cas où il échoue à soumettre un tampon d’interruption
au périphérique. L’impact de sécurité est peu évident.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19227">CVE-2019-19227</a>

<p>Dan Carpenter a signalé des vérifications manquantes dans l’implémentation du
protocole Appletalk, ce qui pourrait conduire à un déréférencement de pointeur
NULL. L’impact de sécurité n’est pas défini.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19332">CVE-2019-19332</a>

<p>L’outil syzkaller a découvert une vérification manquante de limites dans
l’implémentation de KVM pour x86, ce qui pourrait conduire à un dépassement de
tampon de tas. Un utilisateur local autorisé à utiliser KVM pourrait utiliser
cela afin de provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19447">CVE-2019-19447</a>

<p>Il a été découvert que le pilote de système de fichiers ext4 ne gérait pas de
manière sécurisée le détachement d’un inœud qui, à cause de la corruption du
système, a déjà un compte de liens à zéro. Un attaquant capable de monter des
volumes ext4 arbitraires pourrait utiliser cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19523">CVE-2019-19523</a>

<p>L’outil syzkaller a découvert un bogue d’utilisation de mémoire après
libération dans le pilote USB adutux. Un attaquant capable d’ajouter ou de
retirer des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19524">CVE-2019-19524</a>

<p>L’outil syzkaller a découvert une situation de compétition dans la
bibliothèque ff-memless utilisée par les pilotes d’entrée. Un attaquant capable
d’ajouter ou de retirer des périphériques USB pourrait utiliser cela afin de
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19525">CVE-2019-19525</a>

<p>L’outil syzkaller a découvert un bogue d’utilisation de mémoire après
libération dans le pilote atusb pour le réseautage IEEE 802.15.4. Un attaquant
capable d’ajouter ou retirer des périphériques USB pourrait éventuellement
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19527">CVE-2019-19527</a>

<p>L’outil syzkaller a découvert que le pilote hiddev ne gérait pas les
situations de compétition correctement entre une tâche ouvrant le périphérique
et la déconnexion du matériel sous-jacent. Un utilisateur local autorisé à
accéder aux périphériques hiddev et capable d’ajouter ou de retirer des
périphériques USB, pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19530">CVE-2019-19530</a>

<p>L’outil syzkaller a découvert une utilisation potentielle de mémoire après
libération dans le pilote de réseau cdc-acm. Un attaquant capable d’ajouter des
périphériques USB pourrait utiliser cela afin de provoquer un déni de service
(corruption de mémoire ou plantage) ou, éventuellement, pour une élévation des
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19531">CVE-2019-19531</a>

<p>L’outil syzkaller a découvert un bogue d’utilisation de mémoire après
libération dans le pilote USB yurex. Un attaquant capable d’ajouter ou de retirer
des périphériques USB pourrait utiliser cela afin de provoquer un déni de
service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19532">CVE-2019-19532</a>

<p>L’outil syzkaller a découvert un dépassement potentiel de tampon de tas dans le
pilote d’entrée hid-gaff et qui a été aussi trouvé dans beaucoup d’autres pilotes
d’entrée. Un attaquant capable d’ajouter des périphériques USB pourrait utiliser
cela afin de provoquer un déni de service (corruption de mémoire ou plantage)
ou, éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19533">CVE-2019-19533</a>

<p>L’outil syzkaller a découvert que le pilote de média ttusb-dec manquait d’une
initialisation de structure, ce qui pourrait divulguer des informations
sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19534">CVE-2019-19534</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-19535">CVE-2019-19535</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-19536">CVE-2019-19536</a>

<p>L’outil syzkaller a découvert que le pilote CAN peak_usb manquait d’une
initialisation de quelques structures, ce qui pourrait divulguer des
informations sensibles de la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19537">CVE-2019-19537</a>

<p>L’outil syzkaller a découvert des situations de compétition dans la pile USB,
impliquant des enregistrements de périphérique caractère. Un attaquant capable
d’ajouter des périphériques USB pourrait utiliser cela afin de provoquer un déni
de service (corruption de mémoire ou plantage) ou, éventuellement, pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19767">CVE-2019-19767</a>

<p>L’outil syzkaller a découvert que des volumes ext4 contrefaits pourraient
déclencher un dépassement de tampon dans le pilote de système de fichiers ext4.
Un attaquant capable de monter de tels volumes pourrait utiliser cela afin de
provoquer un déni de service (corruption de mémoire ou plantage) ou,
éventuellement, pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19947">CVE-2019-19947</a>

<p>Il a été découvert que le pilote CAN kvaser_usb manquait d’une initialisation
de quelques structures, ce qui pourrait divulguer des informations sensibles de
la mémoire du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19965">CVE-2019-19965</a>

<p>Gao Chuan a signalé une situation de compétition dans la bibliothèque libsas
utilisée par les pilotes SCSI d’hôte, ce qui pourrait conduire à un
déréférencement de pointeur NULL. Un attaquant capable d’ajouter ou de retirer
des périphériques SCSI pourrait utiliser cela afin de provoquer un déni de
service (bogue/oops).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20096">CVE-2019-20096</a>

<p>L’outil Hulk Robot a découvert une fuite potentielle de mémoire dans
l’implémentation du protocole DCCP. Cela peut être exploitable par des
utilisateurs locaux ou par des attaquants distants si le système utilise DCCP,
pour provoquer un déni de service (épuisement de mémoire).</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.210-1~deb8u1. Cette mise à jour corrige de plus les bogues de
Debian <a href="https://bugs.debian.org/869511">n° 869511</a> et <a
href="https://bugs.debian.org/945023">n° 945023</a>, et inclut d’autres
correctifs de bogues à partir de mises à jour de stable, 4.9.190-4.9.210
inclus.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2114.data"
# $Id: $
