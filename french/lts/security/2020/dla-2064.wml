#use wml::debian::translation-check translation="74d3853b7eae0e9c49ae1fcc923d4709cb901c67" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un script d’accroche (hook) pour ldm, le gestionnaire d'affichage du Linux
Terminal Server Project, analysait incorrectement les réponses d'un serveur SSH.
Cela pourrait avoir pour conséquence une élévation de privilèges
administrateur locale.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20373">CVE-2019-20373</a>

<p>LTSP LDM jusqu’à la version 2.18.06 permettait un accès administrateur aux
gros clients parce que la variable <tt>LDM_USERNAME</tt> pouvait avoir une valeur
vide si l’interpréteur de commandes de l’utilisateur ne prenait pas en charge
la syntaxe de l’interpréteur Bourne. Cela concerne un script
<tt>run-x-session</tt>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2:2.2.15-2+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ldm.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2064.data"
# $Id: $
