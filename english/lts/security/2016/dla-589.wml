<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A flaw was discovered in the pdf_load_mesh_params() function allowing
out-of-bounds write access to memory locations. With carefully crafted
input, that could trigger a heap overflow, resulting in application
crash or possibly having other unspecified impact.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.9-2+deb7u3.</p>

<p>We recommend that you upgrade your mupdf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-589.data"
# $Id: $
