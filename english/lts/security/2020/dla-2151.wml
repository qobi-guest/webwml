<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that an integer overflow in the International
Components for Unicode (ICU) library could result in denial of
service and potentially the execution of arbitrary code.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
52.1-8+deb8u8.</p>

<p>We recommend that you upgrade your icu packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2151.data"
# $Id: $
