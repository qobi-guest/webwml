<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An input sanitization bypass was discovered in Wordpress, a popular
content management framework. An attacker can use this flaw to send
malicious scripts to an unsuspecting user.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
4.1.29+dfsg-0+deb8u1.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2067.data"
# $Id: $
