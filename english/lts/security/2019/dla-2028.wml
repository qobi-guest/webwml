<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Squid, a high-performance proxy caching server for
web clients, has been affected by the following security
vulnerabilities.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12526">CVE-2019-12526</a>

     <p>URN response handling in Squid suffers from a heap-based buffer
     overflow. When receiving data from a remote server in response to
     an URN request, Squid fails to ensure that the response can fit
     within the buffer. This leads to attacker controlled data
     overflowing in the heap.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18677">CVE-2019-18677</a>

     <p>When the append_domain setting is used (because the appended
     characters do not properly interact with hostname length
     restrictions), it can inappropriately redirect traffic to origins
     it should not be delivered to. This happens because of incorrect
     message processing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18678">CVE-2019-18678</a>

     <p>A programming error allows attackers to smuggle HTTP requests
     through frontend software to a Squid instance that splits the HTTP
     Request pipeline differently. The resulting Response messages
     corrupt caches (between a client and Squid) with
     attacker-controlled content at arbitrary URLs. Effects are isolated
     to software between the attacker client and Squid.
     There are no effects on Squid itself, nor on any upstream servers.
     The issue is related to a request header containing whitespace
     between a header name and a colon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18679">CVE-2019-18679</a>

     <p>Due to incorrect data management, Squid is vulnerable to
     information disclosure when processing HTTP Digest Authentication.
     Nonce tokens contain the raw byte value of a pointer that sits
     within heap memory allocation. This information reduces ASLR
     protections and may aid attackers isolating memory areas to target
     for remote code execution attacks.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.4.8-6+deb8u9.</p>

<p>We recommend that you upgrade your squid3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2028.data"
# $Id: $
