<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues in libgd2, a graphics library that allows to quickly draw
images, have been found.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6977">CVE-2019-6977</a>

      <p>A potential double free in gdImage*Ptr() has been reported by Solmaz
      Salimi (aka. Rooney).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6978">CVE-2019-6978</a>

      <p>Simon Scannell found a heap-based buffer overflow, exploitable with
      crafted image data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000222">CVE-2018-1000222</a>

      <p>A new double free vulnerabilities in gdImageBmpPtr() has been
      reported by Solmaz Salimi (aka. Rooney).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5711">CVE-2018-5711</a>

      <p>Due to an integer signedness error the GIF core parsing function can
      enter an infinite loop. This will lead to a Denial of Service and
      exhausted server resources.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-5+deb8u12.</p>

<p>We recommend that you upgrade your libgd2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1651.data"
# $Id: $
