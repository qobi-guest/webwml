<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in the Ruby 1.9 interpretor.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0898">CVE-2017-0898</a>

    <p>Buffer underrun vulnerability in Kernel.sprintf</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0899">CVE-2017-0899</a>

    <p>ANSI escape sequence vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0900">CVE-2017-0900</a>

    <p>DOS vulernerability in the query command</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0901">CVE-2017-0901</a>

    <p>gem installer allows a malicious gem to overwrite arbitrary files</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-10784">CVE-2017-10784</a>

    <p>Escape sequence injection vulnerability in the Basic
    authentication of WEBrick</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14033">CVE-2017-14033</a>

    <p>Buffer underrun vulnerability in OpenSSL ASN1 decode</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14064">CVE-2017-14064</a>

    <p>Heap exposure vulnerability in generating JSON</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.3.194-8.1+deb7u6.</p>

<p>We recommend that you upgrade your ruby1.9.1 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1114.data"
# $Id: $
