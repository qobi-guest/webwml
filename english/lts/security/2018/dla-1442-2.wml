<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The security update of mailman announced as DLA-1442-1 introduced a
regression due to an incomplete fix for <a href="https://security-tracker.debian.org/tracker/CVE-2018-13796">CVE-2018-13796</a> that broke the
admin and listinfo overview pages.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1.18-2+deb8u4.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1442-2.data"
# $Id: $
