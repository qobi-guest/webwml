<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability has been discovered in Sympa, a modern mailing list
manager, that allows write access to files on the server filesystem.
This flaw allows to create or modify any file writable by the Sympa
user, located on the server filesystem, using the function of Sympa
web interface template file saving.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
6.1.23~dfsg-2+deb8u2.</p>

<p>We recommend that you upgrade your sympa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1441.data"
# $Id: $
