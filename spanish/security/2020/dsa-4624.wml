#use wml::debian::translation-check translation="8be684d647389ee3db99d941206fa9b5cbef2621"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se descubrieron varias vulnerabilidades en evince, un visor de documentos
multipágina simple.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000159">CVE-2017-1000159</a>

    <p>Tobias Mueller informó de que el exportador a DVI de evince es
    propenso a una vulnerabilidad de inyección de órdenes mediante nombres de fichero
    preparados de una manera determinada.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>

    <p>Andy Nguyen informó de que las funciones tiff_document_render()
    y tiff_document_get_thumbnail() del backend para documentos TIFF
    no gestionaba errores procedentes de TIFFReadRGBAImageOriented(), dando lugar a
    revelación de memoria no inicializada al procesar ficheros con imágenes TIFF.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010006">CVE-2019-1010006</a>

    <p>Una vulnerabilidad de desbordamiento de memoria en el backend tiff podría dar lugar a
    denegación de servicio o, potencialmente, a ejecución de código arbitrario si
    se abre un fichero PDF preparado de una manera determinada.</p></li>

</ul>

<p>Para la distribución «antigua estable» (stretch), estos problemas se han corregido
en la versión 3.22.1-3+deb9u2.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 3.30.2-3+deb10u1. A la distribución «estable» solo le afecta
<a href="https://security-tracker.debian.org/tracker/CVE-2019-11459">CVE-2019-11459</a>.</p>

<p>Le recomendamos que actualice los paquetes de evince.</p>

<p>Para información detallada sobre el estado de seguridad de evince, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/evince">https://security-tracker.debian.org/tracker/evince</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4624.data"
