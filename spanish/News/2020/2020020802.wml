#use wml::debian::translation-check translation="2913230d58de12a2d0daeab2fd1f532a65fa5c2a"
<define-tag pagetitle>Debian 9 actualizado: publicada la versión 9.12</define-tag>
<define-tag release_date>2020-02-08</define-tag>
#use wml::debian::news

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.12</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la duodécima actualización de su
distribución «antigua estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad,
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «antigua estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction base-files "Actualizado para esta versión">
<correction cargo "Nueva versión del proyecto original, para soportar adaptaciones de Firefox ESR; corrige arranque en armhf">
<correction clamav "Nueva versión del proyecto original; corrige problema de denegación de servicio [CVE-2019-15961]; elimina la opción ScanOnAccess, la sustituye por clamonacc">
<correction cups "Corrige validación del idioma por omisión en ippSetValuetag [CVE-2019-2228]">
<correction debian-installer "Recompilado contra oldstable-proposed-updates; establece gfxpayload=keep también en submenús, para corregir tipos de letra ilegibles en pantallas hidpi en imágenes para arranque en red («netboot») arrancadas con EFI; actualizado el valor de USE_UDEBS_FROM por omisión, de unstable a stretch, para ayudar a los usuarios a realizar compilaciones locales">
<correction debian-installer-netboot-images "Recompilado contra stretch-proposed-updates">
<correction debian-security-support "Actualizado el estado del soporte de seguridad de varios paquetes">
<correction dehydrated "Nueva versión del proyecto original; usa API ACMEv2 por omisión">
<correction dispmua "Nueva versión del proyecto original compatible con Thunderbird 68">
<correction dpdk "Nueva versión «estable» del proyecto original; corrige regresión en vhost introducida por la corrección para CVE-2019-14818">
<correction fence-agents "Corrige eliminación incompleta de fence_amt_ws">
<correction fig2dev "Permite que las cadenas de texto Fig v2 terminen con múltiples ^A [CVE-2019-19555]">
<correction flightcrew "Correcciones de seguridad [CVE-2019-13032 CVE-2019-13241]">
<correction freetype "Gestiona correctamente «deltas» en tipos de letra TrueType GX, corrigiendo la visualización en Chromium y Firefox de tipos de letra con «hints» variables">
<correction glib2.0 "Se asegura de que los clientes de libdbus pueden autenticarse con un GDBusServer como el de ibus">
<correction gnustep-base "Corrige vulnerabilidad de amplificación UDP">
<correction italc "Correcciones de seguridad [CVE-2018-15126 CVE-2018-15127 CVE-2018-20019 CVE-2018-20020 CVE-2018-20021 CVE-2018-20022 CVE-2018-20023 CVE-2018-20024 CVE-2018-20748 CVE-2018-20749 CVE-2018-20750 CVE-2018-6307 CVE-2018-7225 CVE-2019-15681]">
<correction libdate-holidays-de-perl "Marca el Día internacional del niño (20 de sep) como festivo en Turingia de 2019 en adelante">
<correction libdatetime-timezone-perl "Actualiza los datos incluidos">
<correction libidn "Corrige vulnerabilidad de denegación de servicio en el tratamiento de Punycode [CVE-2017-14062]">
<correction libjaxen-java "Corrige error de compilación permitiendo fallos en las pruebas">
<correction libofx "Corrige problema de desreferencia de puntero NULL [CVE-2019-9656]">
<correction libole-storage-lite-perl "Corrige interpretación de años del 2020 en adelante">
<correction libparse-win32registry-perl "Corrige interpretación de años del 2020 en adelante">
<correction libperl4-corelibs-perl "Corrige interpretación de años del 2020 en adelante">
<correction libpst "Corrige detección de get_current_dir_name y truncamiento del retorno">
<correction libsixel "Corrige varios problemas de seguridad [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libsolv "Corrige desbordamiento de memoria dinámica («heap») [CVE-2019-20387]">
<correction libtest-mocktime-perl "Corrige interpretación de años del 2020 en adelante">
<correction libtimedate-perl "Corrige interpretación de años del 2020 en adelante">
<correction libvncserver "RFBserver: no filtra contenido de la pila al remoto [CVE-2019-15681]; resuelve una congelación («freeze») durante el cierre de conexión y una violación de acceso en servidores VNC multihilo; corrige problema al conectarse a servidores de VMWare; corrige caída de x11vnc cuando se conecta vncviewer">
<correction libxslt "Corrige puntero colgante en xsltCopyText [CVE-2019-18197]">
<correction limnoria "Corrige revelación de información remota y, posiblemente, ejecución de código remoto en la extensión («plugin») Math [CVE-2019-19010]">
<correction linux "Nueva versión «estable» del proyecto original">
<correction linux-latest "Actualizado para la ABI del núcleo 4.9.0-12">
<correction llvm-toolchain-7 "Inhabilita el enlazador gold de s390x; arranca con -fno-addrsig, el binutils de stretch no funciona con esta opción en mips64el">
<correction mariadb-10.1 "Nueva versión «estable» del proyecto original [CVE-2019-2974 CVE-2020-2574]">
<correction monit "Implementa valor de cookie CSRF independiente de la posición">
<correction node-fstream "Machaca un Link («enlace») si apunta a un File («fichero») [CVE-2019-13173]">
<correction node-mixin-deep "Corrige contaminación de prototipo [CVE-2018-3719 CVE-2019-10746]">
<correction nodejs-mozilla "Nuevo paquete para soportar adaptaciones de Firefox ESR">
<correction nvidia-graphics-drivers-legacy-340xx "Nueva versión «estable» del proyecto original">
<correction nyancat "Recompilado en un entorno limpio para añadir la unidad de systemd para nyancat-server">
<correction openjpeg2 "Corrige desbordamiento de memoria dinámica («heap») [CVE-2018-21010], desbordamiento de entero [CVE-2018-20847] y división por cero [CVE-2016-9112]">
<correction perl "Corrige interpretación de años del 2020 en adelante">
<correction php-horde "Corrige problema de ejecución directa de scrips entre sitios («stored cross-site scripting») en Horde Cloud Block [CVE-2019-12095]">
<correction postfix "Nueva versión «estable» del proyecto original; resuelve mal rendimiento del loopback TCP">
<correction postgresql-9.6 "Nueva versión del proyecto original">
<correction proftpd-dfsg "Corrige desreferencia de puntero NULL en comprobaciones de CRL [CVE-2019-19269]">
<correction pykaraoke "Corrige ruta de tipos de letra">
<correction python-acme "Cambia al protocolo POST-as-GET">
<correction python-cryptography "Corrige errores de la colección de pruebas cuando se compila contra versiones más recientes de OpenSSL">
<correction python-flask-rdf "Corrige dependencias que faltaban en python3-flask-rdf">
<correction python-pgmagick "Gestiona la detección de la versión de actualizaciones de seguridad de graphicsmagick que se identifican a sí mismas como versión 1.4">
<correction python-werkzeug "Se asegura de que los contenedores Docker tengan PIN de depurador únicos [CVE-2019-14806]">
<correction ros-ros-comm "Corrige problema de desbordamiento de memoria [CVE-2019-13566]; corrige desbordamiento de entero [CVE-2019-13445]">
<correction ruby-encryptor "Ignora errores en las pruebas para evitar errores de compilación">
<correction rust-cbindgen "Nuevo paquete para soportar adaptaciones de Firefox ESR">
<correction rustc "Nueva versión del proyecto original, para soportar adaptaciones de Firefox ESR">
<correction safe-rm "Impide la instalación en (y, por lo tanto, la rotura de) entornos con /usr fusionado">
<correction sorl-thumbnail "Solución alternativa para una excepción de pgmagick">
<correction sssd "sysdb: sanea la entrada del filtro de búsqueda [CVE-2017-12173]">
<correction tigervnc "Correcciones de seguridad [CVE-2019-15691 CVE-2019-15692 CVE-2019-15693 CVE-2019-15694 CVE-2019-15695]">
<correction tightvnc "Correcciones de seguridad [CVE-2014-6053 CVE-2018-20021 CVE-2018-20022 CVE-2018-20748 CVE-2018-7225 CVE-2019-8287 CVE-2019-15678 CVE-2019-15679 CVE-2019-15680 CVE-2019-15681]">
<correction tmpreaper "Añade <q>--protect '/tmp/systemd-private*/*'</q> al trabajo cron para evitar la rotura de los servicios systemd que tengan PrivateTmp=true">
<correction tzdata "Nueva versión del proyecto original">
<correction ublock-origin "Nueva versión del proyecto original, compatible con Firefox ESR68">
<correction unhide "Corrige agotamiento de la pila">
<correction x2goclient "Elimina ~/, ~user{,/}, ${HOME}{,/} y $HOME{,/} de las rutas de destino en modo scp; corrige regresión con versiones más recientes de libssh que tengan aplicadas las correcciones para CVE-2019-14889">
<correction xml-security-c "Corrige <q>La verificación de DSA provoca la caída de OpenSSL con combinaciones inválidas del contenido de clave</q>">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «antigua estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2019 4474 firefox-esr>
<dsa 2019 4479 firefox-esr>
<dsa 2019 4509 apache2>
<dsa 2019 4509 subversion>
<dsa 2019 4511 nghttp2>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4522 faad2>
<dsa 2019 4523 thunderbird>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4528 bird>
<dsa 2019 4529 php7.0>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux>
<dsa 2019 4532 spip>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4537 file-roller>
<dsa XXXX 4539 openssl>
<dsa 2019 4540 openssl1.0>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4548 openjdk-8>
<dsa XXXX 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4552 php7.0>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4557 libarchive>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4564 linux>
<dsa 2019 4565 intel-microcode>
<dsa 2019 4567 dpdk>
<dsa 2019 4568 postgresql-common>
<dsa 2019 4569 ghostscript>
<dsa 2019 4571 thunderbird>
<dsa 2019 4573 symfony>
<dsa 2019 4574 redmine>
<dsa 2019 4576 php-imagick>
<dsa 2019 4578 libvpx>
<dsa 2019 4580 firefox-esr>
<dsa 2019 4581 git>
<dsa 2019 4582 davical>
<dsa 2019 4584 spamassassin>
<dsa 2019 4585 thunderbird>
<dsa 2019 4587 ruby2.3>
<dsa 2019 4588 python-ecdsa>
<dsa 2019 4589 debian-edu-config>
<dsa 2019 4590 cyrus-imapd>
<dsa 2019 4591 cyrus-sasl2>
<dsa 2019 4592 mediawiki>
<dsa 2019 4593 freeimage>
<dsa 2019 4594 openssl1.0>
<dsa 2019 4595 debian-lan-config>
<dsa 2019 4596 tomcat8>
<dsa XXXX 4596 tomcat-native>
<dsa 2020 4597 netty>
<dsa 2020 4598 python-django>
<dsa 2020 4600 firefox-esr>
<dsa 2020 4601 ldm>
<dsa 2020 4602 xen>
<dsa 2020 4603 thunderbird>
<dsa 2020 4604 cacti>
<dsa 2020 4607 openconnect>
<dsa 2020 4609 python-apt>
<dsa XXXX 4611 opensmtpd>
<dsa 2020 4612 prosody-modules>
<dsa 2020 4614 sudo>
<dsa 2020 4615 spamassassin>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction firetray "Incompatible con versiones actuales de Thunderbird">
<correction koji "Problemas de seguridad">
<correction python-lamson "Roto por cambios en python-daemon">
<correction radare2 "Problemas de seguridad; el proyecto original no proporciona soporte estable">
<correction ruby-simple-form "No utilizado; problemas de seguridad">
<correction trafficserver "No puede ser soportado">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «antigua estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «antigua estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable/">
</div>

<p>Actualizaciones propuestas a la distribución «antigua estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>Información sobre la distribución «antigua estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
