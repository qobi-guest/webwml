#use wml::debian::translation-check translation="8a5eb3970afae92528c267b43ff2cff70683b130" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3846">CVE-2019-3846</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-10126">CVE-2019-10126</a>

    <p>huangwen rapporterede om adskillige bufferoverløb i wifi-driveren Marvell 
    (mwifiex), hvilken en lokal bruger kunne anvende til at forårsage 
    lammelsesangreb eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5489">CVE-2019-5489</a>

    <p>Daniel Gruss, Erik Kraft, Trishita Tiwari, Michael Schwarz, Ari 
    Trachtenberg, Jason Hennessey, Alex Ionescu og Anders Fogh, opdagede at 
    lokale brugere kunne anvende systemkaldet mincore() til at få fat i 
    følsomme oplysninger fra andre processer, som tilgår den samme 
    memory-mapped fil.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9500">CVE-2019-9500</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2019-9503">CVE-2019-9503</a>

    <p>Hugues Anguelkov opdagede et bufferoverløb og manglende 
    adgangsvalidering i Broadcom FullMAC wifidriveren (brcmfmac), hvilket en 
    angriber på det samme wifi-netværk kunne anvende til at forårsage 
    lammelseangreb eller udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11477">CVE-2019-11477</a>

    <p>Jonathan Looney rapporterede at en særligt fremstillet sekvens af 
    selective TCP-bekræftelser (SACKs) muliggjorde fjernudløsbar 
    kernepanik.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11478">CVE-2019-11478</a>

    <p>Jonathan Looney rapporterede at en særligt fremstillet sekvens af 
    selektive TCP-bekræftelser (SACKs) fragmenterede TCP-gentrasmissionskøen, 
    hvilket gjorde det muligt for en angriber, at forårsage ekstremt stort 
    ressourceforbrug.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11479">CVE-2019-11479</a>

    <p>Jonathan Looney rapporterede at en angriber kunne tvinge Linux-kernen til 
    at segmentere sine svar i adskillige TCP-segmenter, som hver især indeholder 
    kun otte bytes data, hvilket drastisk forøger den påkrævede båndbredde til 
    levering af den samme mængde data.</p>

    <p>Denne opdatering indfører en ny sysctl-værdi til kontrol af den 
    minimale MSS (net.ipv4.tcp_min_snd_mss), hvilket som standard anvender den 
    tidligere hårdkodede værdi 48.  Vi anbefaler at forøge denne til 536, med 
    mindre at dit netværk kræver en lavere værdi.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11486">CVE-2019-11486</a>

    <p>Jann Horn fra Google rapporterede om mange tilfælde af kapløbstilstand i 
    Siemens R3964-linjedisciplin.  En lokal bruger kunne anvende disse til at 
    forårsage ikke-angivne sikkerhedspåvirkning.  Modulet er derfor blevet 
    deaktiveret.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11599">CVE-2019-11599</a>

    <p>Jann Horn fra Google rapporterede om en kapløbstilstand i 
    implementeringen af coredump, hvilken kunne føre til en anvendelse efter 
    frigivelse.  En lokal bruger kunne anvende dette til at læse følsomme 
    oplysninger, til at forårsage et lammelsesangreb (hukommelseskorruption) 
    eller til rettighedsforøgelse.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11815">CVE-2019-11815</a>

    <p>Man opdagede at en anvendelse efter frigivelse i protokollen Reliable 
    Datagram Sockets kunne medføre lammelsesangreb eller potentielt 
    rettighedsforøgelse.  Dette protokolmodul indlæses ikke automatisk på 
    Debian-systemer, hvorfor problemet kun påvirker systemer hvor det 
    eksplicit indlæses.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11833">CVE-2019-11833</a>

    <p>Man opdagede at implementeringen af filsystemet ext4, skriver 
    uinitialiserede data fra kernehukommelsen til nye extent-blokke.  En 
    lokal bruger, der er i stand til at skrive til et ext4-filsystem og 
    derlæst kan læse filsystemsaftrykket, for eksempel ved hjælp af et 
    eksternt drev, kunne måske være i stand tila t anvende det til at få 
    adgang til følsomme oplysninger.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11884">CVE-2019-11884</a>

    <p>Man opdagede at implementeringen af Bluetooth HIDP, ikke sikrede at nye 
    forbindelsesnavne blev null-termineret.  En lokal bruger med kapabiliteten 
    CAP_NET_ADMIN, kunne måske være i standd til at anvende den til at få 
    adgang til følsomme oplysninger fra kernestakken.</p></li>

</ul>

<p>I den stabile distribution (stretch), er disse problemer rettet i
version 4.9.168-1+deb9u3.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingssidede på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4465.data"
