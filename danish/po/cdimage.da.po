msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 17:53+0200\n"
"Last-Translator: unknown\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "    Nøglefingeraftryk"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO-aftryk"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Jigdo-filer"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />OSS"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Hent med Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "Hent via http/ftp"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Køb cd'er eller dvd'er"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Netværksinstallering"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Hent"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Diverse"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Kunst"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Spejling"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Rsync-spejle"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Verificér"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Hent med Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "<void id=\"dc_relinfo\" />Image Release Info"
msgstr "<void id=\"dc_relinfo\" />Aftryksoplysninger"

#: ../../english/template/debian/cdimage.wml:52
msgid "Debian CD team"
msgstr "Debians cd-team"

#: ../../english/template/debian/cdimage.wml:55
msgid "debian_on_cd"
msgstr "debian på cd"

#: ../../english/template/debian/cdimage.wml:58
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />oss"

#: ../../english/template/debian/cdimage.wml:61
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:64
msgid "http_ftp"
msgstr "http/ftp"

#: ../../english/template/debian/cdimage.wml:67
msgid "buy"
msgstr "køb"

#: ../../english/template/debian/cdimage.wml:70
msgid "net_install"
msgstr "netinstall"

#: ../../english/template/debian/cdimage.wml:73
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />div"

#: ../../english/template/debian/cdimage.wml:76
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"Engelsksproget <a href=\"/MailingLists/disclaimer\">offentlig postliste</a> "
"vedr. cd'er/dvd'er:"
