# translation of webwml/english/po/stats.pot to Dutch
# Templates files for webwml modules
# Copyright (C) 2011-2012 Software in the Public Interest, Inc.
#
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2014, 2017, 2018, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: dutch/po/stats.nl.po\n"
"PO-Revision-Date: 2019-02-24 15:49+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../stattrans.pl:278 ../../stattrans.pl:494
msgid "Wrong translation version"
msgstr "Verkeerde vertalingsversie"

#: ../../stattrans.pl:280
msgid "This translation is too out of date"
msgstr "Deze vertaling is te verouderd"

#: ../../stattrans.pl:282
msgid "The original is newer than this translation"
msgstr "Het origineel is nieuwer dan deze vertaling"

#: ../../stattrans.pl:286 ../../stattrans.pl:494
msgid "The original no longer exists"
msgstr "Het origineel bestaat niet meer"

#: ../../stattrans.pl:470
msgid "hits"
msgstr "bezoeken"

#: ../../stattrans.pl:470
msgid "hit count N/A"
msgstr "aantal bezoeken n.v.t"

#: ../../stattrans.pl:488 ../../stattrans.pl:489
msgid "Click to fetch diffstat data"
msgstr "Klik om de verschil-info op te halen"

#: ../../stattrans.pl:599 ../../stattrans.pl:739
msgid "Created with <transstatslink>"
msgstr "Gemaakt met <transstatslink>"

#: ../../stattrans.pl:604
msgid "Translation summary for"
msgstr "Vertaaloverzicht voor"

#: ../../stattrans.pl:607
msgid "Translated"
msgstr "Vertaald"

#: ../../stattrans.pl:607 ../../stattrans.pl:687 ../../stattrans.pl:761
#: ../../stattrans.pl:807 ../../stattrans.pl:850
msgid "Up to date"
msgstr "Bijgewerkt"

#: ../../stattrans.pl:607 ../../stattrans.pl:762 ../../stattrans.pl:808
msgid "Outdated"
msgstr "Verouderd"

#: ../../stattrans.pl:607 ../../stattrans.pl:763 ../../stattrans.pl:809
#: ../../stattrans.pl:852
msgid "Not translated"
msgstr "Niet vertaald"

#: ../../stattrans.pl:608 ../../stattrans.pl:609 ../../stattrans.pl:610
#: ../../stattrans.pl:611
msgid "files"
msgstr "bestanden"

#: ../../stattrans.pl:614 ../../stattrans.pl:615 ../../stattrans.pl:616
#: ../../stattrans.pl:617
msgid "bytes"
msgstr "bytes"

#: ../../stattrans.pl:624
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Let op: De lijst van pagina’s is gesorteerd op volgorde van populariteit. "
"Houd de muis over de paginanaam om het aantal bezoeken te zien."

#: ../../stattrans.pl:630
msgid "Outdated translations"
msgstr "Verouderde vertalingen"

#: ../../stattrans.pl:632 ../../stattrans.pl:686
msgid "File"
msgstr "Bestand"

#: ../../stattrans.pl:634
msgid "Diff"
msgstr "Verschill (diff)"

#: ../../stattrans.pl:636
msgid "Comment"
msgstr "Opmerking"

#: ../../stattrans.pl:637
msgid "Diffstat"
msgstr "Verschil-info"

#: ../../stattrans.pl:638
msgid "Git command line"
msgstr "Git commandoregel"

#: ../../stattrans.pl:640
msgid "Log"
msgstr "Logboek"

#: ../../stattrans.pl:641
msgid "Translation"
msgstr "Vertaling"

#: ../../stattrans.pl:642
msgid "Maintainer"
msgstr "Beheerder"

#: ../../stattrans.pl:644
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:645
msgid "Translator"
msgstr "Vertaler"

#: ../../stattrans.pl:646
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:653
msgid "General pages not translated"
msgstr "Algemene pagina’s die niet zijn vertaald"

#: ../../stattrans.pl:654
msgid "Untranslated general pages"
msgstr "Onvertaalde algemene pagina’s"

#: ../../stattrans.pl:659
msgid "News items not translated"
msgstr "Nieuwsberichten die niet zijn vertaald"

#: ../../stattrans.pl:660
msgid "Untranslated news items"
msgstr "Onvertaalde nieuwsberichten"

#: ../../stattrans.pl:665
msgid "Consultant/user pages not translated"
msgstr "Consultant/gebruikerspagina’s die niet zijn vertaald"

#: ../../stattrans.pl:666
msgid "Untranslated consultant/user pages"
msgstr "Onvertaalde consultant/gebruikerspagina’s"

#: ../../stattrans.pl:671
msgid "International pages not translated"
msgstr "Internationale pagina’s die niet zijn vertaald"

#: ../../stattrans.pl:672
msgid "Untranslated international pages"
msgstr "Onvertaalde internationale pagina’s"

#: ../../stattrans.pl:677
msgid "Translated pages (up-to-date)"
msgstr "Vertaalde pagina’s (bijgewerkt)"

#: ../../stattrans.pl:684 ../../stattrans.pl:834
msgid "Translated templates (PO files)"
msgstr "Vertaalde templates (PO-bestanden)"

#: ../../stattrans.pl:685 ../../stattrans.pl:837
msgid "PO Translation Statistics"
msgstr "PO vertaalstatistieken"

#: ../../stattrans.pl:688 ../../stattrans.pl:851
msgid "Fuzzy"
msgstr "Vaag (fuzzy)"

#: ../../stattrans.pl:689
msgid "Untranslated"
msgstr "Onvertaald"

#: ../../stattrans.pl:690
msgid "Total"
msgstr "Totaal"

#: ../../stattrans.pl:707
msgid "Total:"
msgstr "Totaal:"

#: ../../stattrans.pl:741
msgid "Translated web pages"
msgstr "Vertaalde webpagina’s"

#: ../../stattrans.pl:744
msgid "Translation Statistics by Page Count"
msgstr "Vertaalstatistieken op basis van het aantal pagina’s"

#: ../../stattrans.pl:759 ../../stattrans.pl:805 ../../stattrans.pl:849
msgid "Language"
msgstr "Taal"

#: ../../stattrans.pl:760 ../../stattrans.pl:806
msgid "Translations"
msgstr "Vertalingen"

#: ../../stattrans.pl:787
msgid "Translated web pages (by size)"
msgstr "Vertaalde webpagina’s (grootte)"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Size"
msgstr "Vertaalstatistieken op basis van paginagrootte"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debian website vertaalstatistieken"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Er zijn %d pagina’s te vertalen."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Er zijn %d bytes te vertalen."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Er zijn %d strings te vertalen."

#~ msgid "Unified diff"
#~ msgstr "Verenigde diff"

#~ msgid "Colored diff"
#~ msgstr "Gekleurde diff"

#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Commit diff"

#~ msgid "Created with"
#~ msgstr "Gemaakt met"
