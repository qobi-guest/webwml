#use wml::debian::template title="Förlegad dokumentation"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b"

<h1 id="historical">Historiska dokument</h1>

<p>De dokument som listas nedan skrevs antingen för väldigt länge sedan och
är inte uppdaterade eller har skrivits för tidigare versioner av Debian
och har inte uppdaterats till aktuella versioner. Information i dessa dokument är
gammal, men kan ändå vara av intresse för någon.</p>

<p>De dokument som har förlorat sin relevans och inte har något syfte längre
har fått sina referenser borttagna, men källkoden för dessa föråldrade manualer
finns fortfarande på
<a href="https://salsa.debian.org/ddp-team/attic">DDP's vind</a>.</p>


<h2 id="user">Användaroriented dokumentation</h2>

<document "dselect-dokumentation för nybörjare" "dselect">

<div class="centerblock">
<p>
  Denna fil dokumenterar dselect för förstagångsanvändare och är tänkt som en hjälp
  till att installera Debian framgångsrikt. Den försöker inte förklara
  allt, så när du möter dselect för första gången, arbeta med hjälp av hjälpskärmarna.
</p>
<doctable>
  <authors "St&eacute;phane Bortzmeyer">
  <maintainer "(?)">
  <status>
  avstannad: <a href="https://packages.debian.org/aptitude">aptitude</a> har
  ersatt dselect som Debians standardpakethanteringsgränssnitt
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"
            langs="ca cs da de en es fr hr it ja pl pt ru sk" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Användarguide" "users-guide">

<div class="centerblock">
<p>
Denna <q>Användarguide</q> är inget annat än en omformatterad <q>Användarguide för Progeny</q>.
Innehållet har anpassats till Debians standardsystem.</p>

<p>Över 300 sidor med en bra genomgång kring hur man börjar använda Debiansystemet
från <acronym lang="en" title="Graphical User Interface">GUI</acronym>-skrivbord
och skalkommandoraden.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  Användbar som en genomgång.  Skriven för Woody-utgåvan,
  blir alltmer utdaterad.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debiangenomgång" "tutorial">

<div class="centerblock">
<p>
Denna manual är riktad till en ny Linuxanvändare, för att hjälpa denna att bekanta sig
med Linux efter att de har installerat det, eller till en ny Linuxanvändare på ett system
som någon annan administrerar.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen ersatt av <a href="user-manuals#quick-reference">Debianreferens</a>
  </status>
  <availability>
  inte komplett
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Guide till installation och användning" "guide">

<div class="centerblock">
<p>
  En manual riktad till slutanvändaren.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  färdig (men för potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debians användarreferensmanual" "userref">

<div class="centerblock">
<p>
  Denna manual tillhandahåller åtminstone en överblick kring allt en användare bör veta
  om sitt Debian GNU/Linux-system (d.v.s. sätta upp X, hur man konfigurerar
  nätverket, kommer åt disketter, etc.). Det är menat att fylla glappet
  mellan Debiangenomgången och de detaljerade manual- och infosidor
  som följer med varje paket.</p>

  <p>Den är också menad att ge en idé om hur man kombinerar kommandon, i linje med
  den generella Unixprincipen att <em>det finns alltid mer än ett sätt att
  göra det</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  avstannad och ganska ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />


<document "Debiansystemadministratörsmanual" "system">

<div class="centerblock">
<p>
  Detta dokument nämns i introduktionen till policymanualen.
  Det täcker alla systemadministrationsaspekter av ett Debiansystem.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  ännu inte tillgänglig
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debiannätverksadministratörsmanual" "network">

<div class="centerblock">
<p>
  Denna manual täcker alla nätverksadministrationsaspekter av ett Debiansystem.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  avstannad; ofullständig;
  förmodligen utdaterad av <a href="user-manuals#quick-reference">Debianreferensen</a>
  </status>
  <availability>
  ännu inte tillgänglig
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

# Add this to books, there's a revised edition (2nd) @ Amazon
<document "The Linux Cookbook" "linuxcookbook">

<div class="centerblock">
<p>
  En referensguide till Debian GNU/Linux-systemet som visar,
  med hjälp av fler än 1500 <q>recept</q>, hur man använder det för allehanda daliga aktiviteter &ndash; från
  arbete med text, bilder och ljud till produktivitets- och nätverksfrågor.
  Precis som den mjukvara boken beskriver, är den <q>copyleft</q>
  och dess källkod finns tillgänglig.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  publicerad; skriven för woody, börjar bli utdaterad
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">från författaren</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Denna manual försöker vara en snabb men komplett informationskälla
  om APT-systemet och dess funktioner. Den innehåller mycket information
  om huvudanvändingsområdena för APT och flera exempel.
</p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  förlegad sedan 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <inddpvcs name="apt-howto" langs="en ca cs de es el fr it ja pl pt-br ru uk tr zh-tw zh-cn"
	    formats="html txt pdf ps" naming="locale" vcstype="attic"/>
  </availability>
</doctable>
</div>


<h2 id="devel">Utvecklardokumentation</h2>

<document "Introduktion: Att göra ett Debianpaket" "makeadeb">

<div class="centerblock">
<p>
  Introduktion om hur man gör en <code>.deb</code> med hjälp av
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  avstannad, utdaterad av <a href="devel-manuals#maint-guide">Debian New Maintainers' Guide</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debianprogrammerarens manual" "programmers">

<div class="centerblock">
<p>
  Hjälper nya utvecklare att skapa ett paket för Debian GNU/Linux-systemet.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  utdaterad av <a href="devel-manuals#maint-guide">New Maintainers' Guide</a>
  </status>
  <availability>
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debians paketeringshandbok" "packman">

<div class="centerblock">
<p>
  Denna handbok beskriver de tekniska aspekterna vad gäller att skapa
  binär- och källkodspaket för Debian.
  Den beskriver även gränssnittet mellan dselect och dess åtkomstskript.
  Den hanterar inte Debianprojektets policykrav, och förutsätter
  förtrogenhet med dpkgs funktioner från systemadministratörens synvinkel.

<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Delar som var de facto-policy flyttades nyligen in i 
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
    <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<

<document "Hur programvaruproducenter kan distribuera sina program direkt i .deb-formatet" "swprod">

<div class="centerblock">
<p>
 Detta dokument är avsedd som en startpunkt för att förklara hur
 programvaruproducenter kan integrera sina produkter med Debian, vilka olika
 situationer kan uppkomma beroende på produktens licens och producenternas
 olika val, samt vilka möjligheter som finns.
 Den förklarar inte hur man skapar paket, utan länkar till dokument som gör
 precis det.

 <p>Du bör läsa denna om du inte känner till de övergripande dragen i att
 skapa och distribuera Debianpaket, och möjligen även med att lägga dem till
 Debiandistributionen.

<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  klar (?)
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Introduction to i18n" "i18n">

<div class="centerblock">
<p>
  Detta dokument beskriver den grundläggande idén med och hur man utför
  lokalanpassning (<span lang=en>l10n, localization</span>),
  internationalisering (<span lang=en>i18n, internationalization</span>),
  och flerspråksanpassning (<span lang=en>m17n, multilingualization</span>)
  för programmerare och paketansvariga.

  <p>Målet med detta dokument är att få fler paket att stöda
  internationalisering och göra Debian en mer internationaliserad
  distribution.
  Bidrag från hela världen välkomnas då originalförfattaren är
  japansktalande och detta dokument skulle handla om japanskanpassning om
  det inte funnes några bidragslämnare.

<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  avstannad, inaktuell
  </status>
  <availability>
  ännu ej klar
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr />

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  Detta HOWTO-dokument innehåller praktisk information om hur man använder
  SGML och XML på operativsystemet Debian.

<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  avstannat, inaktuellt
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto"
            formats="html"
            srctype="SGML"
            vcstype="attic"
  />
  </availability>
 </doctable>
 </div>

<hr>

<document "Debians riktlinjer för XML/SGML" "xml-sgml-policy">

<div class="centerblock">
<p>
  Delpolicy för Debianpaket som tillhandahåller och/eller använder
  XML- eller SGML-resurser.

<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  igångsatt, tar in nuvarande SGML-riktlinjer från <tt>sgml-base-doc</tt>
  samt nytt material för XML-kataloghantering
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<document "DebianDoc-SGML Markup Manual" "markup">

<div class="centerblock">
<p>
  Dokumentation för <strong>debiandoc-sgml</strong>-systemet,
  inklusive bästa sätt att utföra saker på och tips för utvecklare.
  Framtida versioner bör innehålla tips om förenklat underhåll och
  för att bygga dokumentation i Debianpaket, riktlinjer för att
  organisera översättningar av dokumentation, och annan hjälpsam
  information.
  Se även <a href="https://bugs.debian.org/43718">felrapport 43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  klar
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<hr>

<h2 id="misc">Blandad dokumentation</h2>

<document "Guide till Debianarkiv" "repo">

<div class="centerblock">
<p>
  Detta dokument förklarar hur Debianarkiv fungerar, hur man skapar dem,
  samt hur man lägger dem till <tt>sources.list</tt> på rätt sätt.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  klar (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
            formats="html" langs="en fr de uk ta" vcstype="attic">
  </availability>
</doctable>
</div>
